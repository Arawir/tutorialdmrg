#include <iostream>
#include <armadillo>
#include <iomanip>

#define uint arma::uword
#define output std::cout << std::fixed << std::setprecision(4)
#define input std::cin

arma::mat operator &(arma::mat A, arma::mat B)
{
    return kron(A, B);
}

arma::mat I(uint rank)
{
    if( rank<=0 ){ return 0; }
    return arma::mat{rank, rank, arma::fill::eye};
}

arma::mat sigmaX()
{
    arma::mat SigmaX{2,2,arma::fill::zeros};
    SigmaX(0,0) = 0.0;
    SigmaX(0,1) = 1.0;
    SigmaX(1,0) = 1.0;
    SigmaX(1,1) = 0.0;

    return SigmaX;
}

arma::mat sigmaZ()
{
    arma::mat SigmaZ{2,2,arma::fill::zeros};
    SigmaZ(0,0) = 1.0;
    SigmaZ(0,1) = 0.0;
    SigmaZ(1,0) = 0.0;
    SigmaZ(1,1) = -1.0;

    return SigmaZ;
}

arma::mat generateIsingHamiltonian(uint numberOfNodes, double J, double K)
{
    arma::mat Hamiltonian{ static_cast<uint>(pow(2,numberOfNodes)),
                           static_cast<uint>(pow(2,numberOfNodes)),
                           arma::fill::zeros };

    for(uint i=0; i<numberOfNodes-1; i++){
        Hamiltonian += -J * I(pow(2,i)) & sigmaZ() & sigmaZ() & I(pow(2, numberOfNodes-2-i));
    }

    for(uint i=0; i<numberOfNodes; i++){
        Hamiltonian += K * I(pow(2,i)) & sigmaX() & I(pow(2, numberOfNodes-1-i));
    }

    return Hamiltonian;
}

int main()
{
    uint NumberOfNodes = 1;
    double J = 1.0;
    double K = 1.0;

    while(1){
        output << "Select number of nodes: ";
        input >> NumberOfNodes;

        arma::mat Hamiltonian = generateIsingHamiltonian(NumberOfNodes, J, K);

        output << "Hamiltonian of " << NumberOfNodes << " nodes system" << std::endl
               << Hamiltonian << std::endl
               << std::endl;
    }
    return 0;
}

